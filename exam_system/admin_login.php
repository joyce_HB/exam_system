<?php
session_start();
include "perfect_function.php";

?>

<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Register to test maker</title>
  </head>

 
  <body style="background: linear-gradient(90deg, rgba(247,234,249,1) 8%, rgba(252,250,252,1) 52%, rgba(232,243,252,1) 96%);">
    <div style="background: linear-gradient(90deg, rgba(242,232,243,1) 7%, rgba(254,254,254,1) 32%, rgba(253,253,253,1) 64%, rgba(246,239,248,1) 96%);">
      <center>
        <h1 style=" font-size: 50px;
  background: -webkit-linear-gradient(#eee, #333);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;">
          <img src="image/icon.png" width="50px">
          Welcome to Test Maker!
          <h6><i>The easiest and fastest way of creating your test online.</i></h6>
        </h1>
      </center>
        <hr style="border:2px solid; color:darkgray">
    
    </div> <!--header-->

    <ul class="nav nav-tabs">
  
  <li class="nav-item">
    <a class="nav-link active" href="index.php">HOME</a>
  </li>

  
</ul>
    

<div class="container col-md-5" style="border: 1px solid gray;">
        <div class="container " style="background:  linear-gradient(90deg, rgba(250,245,250,1) 0%, rgba(231,235,238,1) 47%, rgba(248,250,250,1) 96%);">
            
            <center><h3>LOGIN</h3></center>
            <br>
            <form action="admin_login_proc.php" method="post">
            <div class="row">
            <div class="col-3"></div>
                    
          
            <?php
                    if (isset ($_SESSION['alert_msg'])){
                      
                      if ($_SESSION['alert_msg']=1){
                        echo ' <center>
                        <p class="text-danger">INCORRECT USERNAME OR PASSWORD!</p>
                        </center>      
                            
                        ';
                        unset($_SESSION['alert_msg']);
                  }
                       
                        
                  }

                ?> 
          
                   
                </div>
           
            
                <div class="row">
                    
                    <div class="col-3"></div>
                    <div class="col-6">
                        <label for="form-control">Username</label>
                        <input type="text" class="form-control" name="username" required>
                    </div>
                    
                   
                </div>
                <br>
                <div class="row">
                    
                    <div class="col-3"></div>
                    <div class="col-6">
                        <label for="form-control">Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    
           
                </div>
                

                <br>
                <div class="row">
                    
                    <div class="col-5"></div>
                    <BUTton class="btn btn-primary" role="button" >
                    LOGIN
                   </BUTton> 
    
                </div>
                <BR>
                <div class="row">
 
                </div>
                <br>
                 
                <div class="row">
                    
                    <div class="col-2"></div>
                    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Don't have an account? 
                    <a href="admin_register.php">
                     &nbsp;REGISTER NOW!
                    </a>
                    
                    
    
                </div>
                <BR>
                <div class="row">
 
                </div>

            </form>
        </div>
</div>
  





























    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>