<?php
session_start();
include 'perfect_function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body style="background: linear-gradient(90deg, rgba(204,196,208,1) 0%, rgba(253,255,255,1) 48%, rgba(201,194,208,1) 100%);">



<!----navbar---->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:white;">
  <a class="navbar-brand" href="index.php" style="color: #f1f8e9">Test Maker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="admin_page.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_page.php">Test Created</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_taker_page.php">Test Takers</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <?php
        $table_name = "admin";
        if (isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            
        }
        $mysql_query = "select firstname, lastname from admin where id = '$id'  "; 
        $data = custom_query($mysql_query);

        foreach ($data as $key => $row){
            
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];

            echo '<p style="color: #59698d">'.'Welcome, '. $firstname.' '.$lastname.'  '.'<br>'.'</p>';
            
        }


        $title_query = "SELECT * FROM `title`";
        $title_data = custom_query($title_query);
        $option = "";
        foreach ($title_data as $key => $row){ 
          $title = $row['title'];
        
          $option = $option."<option value ='$title'>".$title."</option>";
     
        }
    
      ?>
      
      
    </form>
    <a href=logout.php class="btn btn-outline-success my-2 my-sm-0 btn-sm btn-outline-secondary" role="button"> Logout</a>
  </div>
</nav>

  <HR>
<div class="container col-md-8">
<?php
                    if (isset ($_SESSION['create'])){
                      
                      if ($_SESSION['create']=1){
                        echo ' <center>
                        <p class="text-success">A new test was added<br>
                        <br>
                       
                        </p>
                        </center>      
                            
                        ';
                        unset($_SESSION['create']);
                  }
                       
                        
                  }

                ?>
    <div class="card text-center">
      <div class="card-header text-center">
      <h4 style="color: #2BBBAD">TEST TAKER</h4>
      </div>
      <div class="card-body">
      
          <form action="" method="post">

          
          <div class="form-group col-md-12">
          <table class="table table-borderless">
  <thead>
    <tr>
    <th scope="col">NAME</th>
    <th scope="col">TEST NAME/TITLE</th>
      <th scope="col">TYPE OF QUESTION</th>
      <th scope="col">SCORE</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
  
             <?php
                $taker_name="select id, firstname, lastname from takers";
                $name_data = custom_query($taker_name);
                foreach ($name_data as $key => $row){
                    $tname= $row['id'];
                    $fname= $row['firstname'];
                    $lname =$row['lastname'];
                  

                 $taker_query = "select id, title_id, taker_id, type_id , score from test_taker where taker_id = $tname";
                 $taker_data = custom_query($taker_query);

                  foreach ($taker_data as $key => $row){
                      $id = $row['id'];
                      $title_id = $row['title_id'];
                      $taker_id = $row['taker_id'];
                      $type_id = $row['type_id'];
                      $score = $row['score'];


                      $title_query ="select id, title from title where id = $title_id ";
                      $title_data = custom_query($title_query);
                      
                      foreach ($title_data as $key => $row){
                        $title_id = $row['id'];
                        $title = $row['title'];
               
               
                        
                      ?>
                      <tr>
                        <td><!--name -->

                        <?php
                          echo $fname." ".$lname."<br>";
                          ?>
                        </td>

                        <td><!--test title-->
                        <?php
                          echo $title."<br>";
                          ?>
                        </td>

                        <td><!--QUESTION TYPE-->
                          
                        <?php
                          echo $type_id."<br>";
                          ?>
                        </td>

                        <td><!--score-->
                        <?php
                          echo $score."<br>";
                          ?>
                        </td>
                      </tr>
                <?php
                    }
                                    
                    }
                }
                      
                     
                    ?>
               
      
   
  </tbody>
</table>
         


          </div>

          

          </div>


      
        </form>




        
      </div>
     
    
    </div>

    
</div>


</body>
</html>
