<?php
	include "perfect_function.php";

	$table_name = 'test';

	//get user ID from URL
    $id = $_GET['id'];
    $testid = $_GET['testid'];
	$question= $_POST['q1'];
        
        $choice1 = $_POST['a'];
        $choice2 = $_POST['b'];
        $choice3 = $_POST['c'];

        $answer = $_POST['correct'];
        if ($answer == "a"){
            $answer = $choice1;
        }elseif ($answer == "b"){
            $answer = $choice2;
        }elseif ($answer == "c"){
            $answer = $choice3;
        }


	$user_editedValues = array(
        
        "question"=> $question,
        "answer"=> $answer,
        
        "choice1" => $choice1,
        "choice2" => $choice2,
        "choice3" => $choice3
	
	);

    echo update($user_editedValues, $testid, $table_name);
    
    $_SESSION['updated']=1;
	header("Location: multiple_choice_edit.php?titleid=$id&testid=$testid ");
?>