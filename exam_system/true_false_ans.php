<?php
session_start();
include 'perfect_function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body style="background: linear-gradient(90deg, rgba(204,196,208,1) 0%, rgba(253,255,255,1) 48%, rgba(201,194,208,1) 100%);">



<!----navbar---->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:white;">
  <a class="navbar-brand" href="index.php" style="color: #f1f8e9">Test Maker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="takers_page.php">Home <span class="sr-only">(current)</span></a>
      </li>
      
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <?php
        $table_name = "takers";
        if (isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            
        }
        $mysql_query = "select firstname, lastname from takers where id = '$id'  "; 
        $data = custom_query($mysql_query);

        foreach ($data as $key => $row){
            
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];

            echo '<p style="color: #59698d">'.'Welcome, '. $firstname.' '.$lastname.'  '.'<br>'.'</p>';
            
        }


    
      ?>
      
      
    </form>
    <a href=logout.php class="btn btn-outline-success my-2 my-sm-0 btn-sm btn-outline-secondary" role="button"> Logout</a>
  </div>
</nav>

  <HR>
<div class="container col-md-6">
<?php
                    if (isset ($_SESSION['submit'])){
                      
                      if ($_SESSION['submit']=1){
                        echo ' <center>
                        <h4 class="text-danger">Thank you for answering!</h4>
                        </center>      
                            
                        ';
                        unset($_SESSION['submit']);
                  }
                       
                        
                  }

                ?>

<?php
                    if (isset ($_SESSION['answers'])){

                      
                      
                      if ($_SESSION['answers']=9){
                        
                        $score_query = "select taker_id, score from test_taker where taker_id = $id";
                        $score_data = custom_query($score_query);

                        foreach ($score_data as $key =>$row){
                          $takers_id = $row['taker_id'];
                          $score = $row['score'];

                          
                          
                        }
                        echo ' <center>
                        <h3 class="text-info">Your score is: '.$score.'</h3>
                        
                        </center>      
                            
                        ';
                        
                        
                        
                        unset($_SESSION['answers']);
                  }
                       
                        
                  }

                ?>
<div class="card text">
                
  <div class="card-header text-center">
  <h4 style="color: #2BBBAD">TRUE OR FALSE</h4> 
  </div>
  <div class="card-body">

  <?php

    $title_id = $_GET['title_id'];
    $type_id = $_GET['type_id'];
  ?>

  <form method="post" action="true_false_ans_proc.php?id=<?=$id?>?&type_id=<?=$type_id?>?&title_id=<?=$title_id?>">

     
  <div class="form-group col-md-12">
    
    <?php
   
    $mysql_query = "select id, title_id, question from test where title_id = $title_id && type = 'tf'  "; 
    $data = custom_query($mysql_query);


      $number = 0;
      foreach ($data as $key => $row){
        $question_id = $row['id'];
        $question= $row['question'];
        
        $number = $number +1;
    ?>
    <div class = "row">
      <?php
          echo "<br>".$number.". ".$question;

          $choice = "Select choice1, choice2 from test where id = '$question_id'";
          $data2 = custom_query($choice);
          foreach($data2 as $key =>$row){
            $choice1 = $row['choice1'];
            $choice2 = $row['choice2'];
            
            

          ?>
      <div class="form-group col-md-12">
          
          <div class="unit_box"><div class="answer">
                <div class="answer_heading">
                  
                  <input class="showCorrect mc_forminput" type="radio" name="answer<?=$number?>" value="<?=$choice1?>" id="a1">
                  <LAbel>A. <?=$choice1?> </LAbel>  
                  
                  
                  
                </div>
                
          </div>

          <div class="unit_box"><div class="answer">
                <div class="answer_heading">
                  
                  <input class="showCorrect mc_forminput" type="radio" name="answer<?=$number?>" value="<?=$choice2?>" id="a1">
                  <LAbel>B. <?=$choice2?> </LAbel>  
                  
                  
                  
                </div>
                
          </div>
          
          
            

      
      
        <?php   } ?>
        <?php
          }
        ?>
        
        


      
      </div>
      <br>
    </div>
  
  <div class="form-group col-md-8">
        <button class="btn btn-success " name="submit">SUBMIT</button>
        <a href="takers_page.php?id=<?=$id?>">CANCEL</a>
        
  </div>
  
  
</form>
  </div>
 
</div>
</div>

</body>
</html>
