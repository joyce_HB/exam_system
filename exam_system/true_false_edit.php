<?php
session_start();
include 'perfect_function.php';

?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body style="background: linear-gradient(90deg, rgba(204,196,208,1) 0%, rgba(253,255,255,1) 48%, rgba(201,194,208,1) 100%);">



<!----navbar---->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:white;">
  <a class="navbar-brand" href="index.php" style="color: #f1f8e9">Test Maker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="admin_page.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_page.php">Test Created</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_taker_page.php">Test Takers</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <?php
        $table_name = "admin";
        if (isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            
        }
        $mysql_query = "select firstname, lastname from admin where id = '$id'  "; 
        $data = custom_query($mysql_query);

        foreach ($data as $key => $row){
            
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];

            echo '<p style="color: #59698d">'.'Welcome, '. $firstname.' '.$lastname.'  '.'<br>'.'</p>';
            
        }


    
      ?>
      
      
    </form>
    <a href=logout.php class="btn btn-outline-success my-2 my-sm-0 btn-sm btn-outline-secondary" role="button"> Logout</a>
  </div>
</nav>

  <HR>
<div class="container col-md-5">
<?php
                    if (isset ($_SESSION['update'])){
                      
                      if ($_SESSION['update']=1){
                        echo ' <center>
                        <p class="text-success">Updated succesfully!. <br>
                        Add another question? <br>
                        <a class="text-danger" href="test_type.php?id=<?=$id?>">CANCEL</a>

                        </p>
                        </center>      
                            
                        ';
                        unset($_SESSION['update']);
                  }
                       
                        
                  }

                ?>

                
    <?php

$testid = $_GET['testid'];
$titleid = $_GET['titleid'];
$form_location = "true_false_edit_proc.php?testid=".$testid."&titleid".$titleid;

$table_name = "test";
$get_userData = get_where($table_name, $testid);

foreach ($get_userData as $key => $row) {
	  $testid = $row['id'];
    $question = $row['question'];
    $choice1 = $row['choice1'];
    $choice2 = $row['choice2'];

    $answer = $row['answer'];

	
}
?>
<div class="card text">

  <div class="card-header text-center">
  <h5 style="color: #2BBBAD ">EDIT QUESTION</h5>
  </div>
  <div class="card-body">
  <form method="post" action="<?=$form_location?>">

     
  <div class="form-group col-md-12">
    <label for="formGroupExampleInput">Type your question below:</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="q1" value="<?=$question?>" >
  </div>
  <div class="form-group col-md-12">
    <label for="formGroupExampleInput"></label>
    <div class="unit_box"><div class="answer">
					<div class="answer_heading">
						<h5 class="">(A)</h5>
						<input class="showCorrect mc_forminput" type="radio" name="correct" value = "a" id="a1" required >
						<label for="a1" class="">This answer option is correct</label>
		</div>
          <input type="text" class="form-control" id="formGroupExampleInput" name="a"  value="<?=$choice1?> ">
          <br>
    <div class="unit_box"><div class="answer">
					<div class="answer_heading">
						<h5 class="">(B)</h5>
						<input class="showCorrect mc_forminput" type="radio" name="correct"  value = "b" id="a2" required>
						<label for="a2" class="">This answer option is correct</label>
		</div>
         <input type="text" class="form-control" id="formGroupExampleInput" name="b" value ="<?=$choice2?>">


  </div>
  <br>

  
  


<br>
  
  <div class="form-group col-md-8">
        <button class="btn btn-success " name="update">UPDATE</button>
        <a href="test_page.php?testid=<?=$testid?>">CANCEL</a>
        
  </div>
  
  
</form>






    
  </div>
 
</div>
</div>

</body>
</html>
