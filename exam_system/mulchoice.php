<?php
session_start();
include 'perfect_function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">

    <div class="navbar-header">
    
      <a class="navbar-brand" href="index.php">Test Maker</a>
      
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.php">Home</a></li>
      
     
      
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span>
      <?php
    $table_name = "admin";
    if (isset($_SESSION['id'])){
        $id = $_SESSION['id'];
        
    }
    $mysql_query = "select firstname, lastname from admin where id = '$id'  "; 
    $data = custom_query($mysql_query);

    foreach ($data as $key => $row){
        
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];

        echo "Welcome, ". $firstname." ".$lastname."<br>";
    }
    
?>
    </a>

    </li>
      
      <li><a href=logout.php> Logout</a></li>
    </ul>
  </div>
</nav>

  <HR>
<div class="container col-md-5">
<div class="card text">
  <div class="card-header text-center">
    CREATE A TEST
  </div>
  <div class="card-body">
  <form method="post" action="create_proc.php">
      
  <div class="form-group col-md-12">
    <label for="formGroupExampleInput">Type your question here</label>
    <input type="text" class="form-control" id="formGroupExampleInput" name="q1" required >
  </div>
  <div class="form-group col-md-12">
    <label for="formGroupExampleInput">Type your answer here</label>
    <div class="unit_box"><div class="answer">
					<div class="answer_heading">
						<h5 class="">(A)</h5>
						<input class="showCorrect mc_forminput" type="checkbox" name="correct1" value="1" id="a1">
						<label for="a1" class="">This answer option is correct</label>
					</div>
    <input type="text" class="form-control" id="formGroupExampleInput" name="ans1check" required >
    <div class="unit_box"><div class="answer">
					<div class="answer_heading">
						<h5 class="">(B)</h5>
						<input class="showCorrect mc_forminput" type="checkbox" name="correct2" value="2" id="a2">
						<label for="a2" class="">This answer option is correct</label>
					</div>
    <input type="text" class="form-control" id="formGroupExampleInput" name="ans1" required >
  </div>
  


  <div class="form-group col-md-8">
        <button class="btn btn-success ">ADD MORE CHOICES</button>
  </div>
  
</form>






    
  </div>
 
</div>
</div>

</body>
</html>
