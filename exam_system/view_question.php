<?php
session_start();
include 'perfect_function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body style="background: linear-gradient(90deg, rgba(204,196,208,1) 0%, rgba(253,255,255,1) 48%, rgba(201,194,208,1) 100%);">



<!----navbar---->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:white;">
  <a class="navbar-brand" href="index.php" style="color: #f1f8e9">Test Maker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="admin_page.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_page.php">Test Created</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="test_taker_page.php">Test Takers</a>
      </li>
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <?php
        $table_name = "admin";
        if (isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            
        }
        $mysql_query = "select firstname, lastname from admin where id = '$id'  "; 
        $data = custom_query($mysql_query);

        foreach ($data as $key => $row){
            
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];

            echo '<p style="color: #59698d">'.'Welcome, '. $firstname.' '.$lastname.'  '.'<br>'.'</p>';
            
        }


    
      ?>
      
      
    </form>
    <a href=logout.php class="btn btn-outline-success my-2 my-sm-0 btn-sm btn-outline-secondary" role="button"> Logout</a>
  </div>
</nav>

  <HR>


<div class="container col-md-10">
    <div class="card-header text-center">
    <h5 style="color: #2BBBAD ">TEST NAME:</h5>
        <?php
          $id = $_GET['title_id'];
          $id_query = "select id, title from title where id = $id";
          $id_data = custom_query($id_query);

          foreach ($id_data as $key => $row){
              $id = $row['id'];
              $title = $row['title'];

              echo '<h5 style="color: #2BBBAD ">'.$title.''.'</h5>';
            }

                  $test_query ="select id, title from title where id = '$id'";
                  $test_data = custom_query($test_query);

                  foreach ($test_data as $key => $row){
                    $test_id = $row['id'];
                    $test_title = $row['title'];
                  

                    
                  
                }
             ?>
    </div>
    
           

        

    <div class="container col-md-12">

        <div class="col-sm-12">
            <div class="card">
            <div class="card-body">
           <center> 
                <h5 style="color:#e57373 ">MULTIPLE CHOICE</h5>
           </center>
            <hr>
            
                <table class="table table-borderless">
                    <thead>
                        <tr>
                        
                        <th scope="col">Question</th>
                        <th scope="col">Answer</th>
                        <th scope="col">Options</th>
                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">
                        <?php
                                $q_query ="select id, title_id, answer, question, type from test where title_id = '$test_id' && type= 'mc' ";
                                $q_data = custom_query($q_query);

                                  $num = 0;
                                  foreach ($q_data as $key => $row){
                                  $id_test = $row['id'];
                                  $name = $row ['title_id'];
                                  $type = $row['type'];
                                  $question = $row['question'];
                                  $answer = $row['answer'];
   
                                  $num=$num+1;

                                 
                            ?>

                        <tr>
                            <td>
                                <?php

                                  echo $num.". ";
                                  echo $question."<br>";
                                  ?>
                            </td>

                            <td>
                                  <?php
                                      echo $answer;
                                      
                                  ?>
                            </td>
                            <td>
                                    <a href="multiple_choice_edit.php?titleid=<?=$test_id?>&testid=<?=$id_test?>" class="btn btn-info">EDIT</a>
                                    <a href="delete.php?testid=<?=$id_test?>" class="btn btn-danger">DELETE</a>
                                  
                            </td>

                       
                            
                            
                        
                                
                        </tr>

                    <?php 
                       } ?>
                        
                    </tbody>
                    </table>
            </div>
            </div>
        </div>
</div>
<br>
<div class="container col-md-12">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-body">
            <center> <h5 style="color:#e57373 ">TRUE OR FALSE</h5></center>
            <hr>
            <table class="table table-borderless">
                    <thead>
                        <tr>
                        
                        <th scope="col">Question</th>
                        <th scope="col">Answer</th>
                        <th scope="col">Options</th>
                        
                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                        <th scope="row">
                        <?php
                                $q_query ="select id, title_id, answer, question, type from test where title_id = '$test_id' && type= 'tf' ";
                                $q_data = custom_query($q_query);

                                  $num = 0;
                                  foreach ($q_data as $key => $row){
                                  $id_test = $row['id'];
                                  $name = $row ['title_id'];
                                  $type = $row['type'];
                                  $question = $row['question'];
                                  $answer = $row['answer'];
   
                                  $num=$num+1;

                                 
                            ?>

                        <tr>
                            <td>
                                <?php

                                  echo $num.". ";
                                  echo $question."<br>";
                                  ?>
                            </td>

                            <td>
                                  <?php
                                      echo $answer;
                                  ?>
                            </td>

                            <td>
                                    <a href="true_false_edit.php?titleid=<?=$test_id?>&testid=<?=$id_test?>" class="btn btn-info">EDIT</a>
                                    <a href="delete.php?testid=<?=$id_test?>" class="btn btn-danger">DELETE</a>
                                    
                            </td>
                            
                            
                        
                                
                        </tr>

                    <?php 
                       } ?>
                        
                    </tbody>
                    </table>
            </div>
            </div>
        </div>
        </div>
    </div>

</div>
</div>

<br>

<br>


</body>
</html>
