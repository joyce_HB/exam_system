<!doctype html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
  </head>

  <body style="background: linear-gradient(90deg, rgba(247,234,249,1) 8%, rgba(252,250,252,1) 52%, rgba(232,243,252,1) 96%);">
    <div style="background: linear-gradient(90deg, rgba(242,232,243,1) 7%, rgba(254,254,254,1) 32%, rgba(253,253,253,1) 64%, rgba(246,239,248,1) 96%);">
      <center>
        <h1 style=" font-size: 50px;
  background: -webkit-linear-gradient(#eee, #333);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;">
          <img src="image/icon.png" width="50px">
          Welcome to Test Maker!
          <h6><i>The easiest and fastest way of creating your test online.</i></h6>
        </h1>
      </center>
        <hr style="border:2px solid; color:darkgray">
    
    </div> <!--header-->






<br><br>


    <div class="container"> <!--admintakers-->
      

      <div class="row">
          <div class="col"> <!--admin-->
            <TABle class="table-bordered">
            <tr>
              <td class="table-info">
              Register to create tests
              
                
              </td>
            </tr>
      
            <tr class="admin">
              
              <td class="label">
              <br>
              <a href="admin_login.php">ADMINISTRATORS</a>
              
            
              </td>
            </tr>
            
            
            
            </TABle>
            
          </div><!--admin end-->

          <br>
          <div class="col"> <!--STUDENT-->
            <TABle class="table-bordered">
            <tr>
              <td class="table-info">
              Register to take test
              
                
              </td>
            </tr>
      
            <tr class="taker">
              
              <td class="label">
              <br>
              <a href="test_taker_login.php">TEST TAKERS</a>
              
              </td>
            </tr>
            
            
            </TABle>
            
          </div> <!--STUDENT end-->

      </div>

    </div> <!--admintakers end-->

   





























    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 
 
 
 
 
 
 
  </body>
</html>