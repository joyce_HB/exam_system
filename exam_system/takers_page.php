<?php
session_start();
include 'perfect_function.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel = "icon" type = "image/png" href = "image/icon.png">
    <title>Welcome to Test Maker!</title>
</head>
<body style="background: linear-gradient(90deg, rgba(204,196,208,1) 0%, rgba(253,255,255,1) 48%, rgba(201,194,208,1) 100%);">



<!----navbar---->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:white;">
  <a class="navbar-brand" href="index.php" style="color: #f1f8e9">Test Maker</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="takers_page.php">Home <span class="sr-only">(current)</span></a>
      </li>
      
      
    </ul>
    <form class="form-inline my-2 my-lg-0">
    <?php
        $table_name = "takers";
        if (isset($_SESSION['id'])){
            $id = $_SESSION['id'];
            
        }
        $mysql_query = "select firstname, lastname from takers where id = '$id'  "; 
        $data = custom_query($mysql_query);

        foreach ($data as $key => $row){
            
            $firstname = $row['firstname'];
            $lastname = $row['lastname'];

            echo '<p style="color: #59698d">'.'Welcome, '. $firstname.' '.$lastname.'  '.'<br>'.'</p>';
            
        }


        $title_query = "SELECT * FROM `title`";
        $title_data = custom_query($title_query);
        $option = "";
        foreach ($title_data as $key => $row){ 
          $title = $row['title'];
        
          $option = $option."<option value ='$title'>".$title."</option>";
     
        }
    
      ?>
      
      
    </form>
    <a href=logout.php class="btn btn-outline-success my-2 my-sm-0 btn-sm btn-outline-secondary" role="button"> Logout</a>
  </div>
</nav>

  <HR>
<div class="container col-md-5">
<div class="card text">
  <div class="card-header text-center ">
   <h4 style="color: #2BBBAD">TAKE A TEST</h4> 
  </div>
  <div class="form-group col-md-12">
              <label for="inputState" style="font-size:18px; color:#8d6e63;" >Choose topic to answer:</label><br>
                      
           
            
  </div>
  
  <div class="card-body">
  <?php
  $title_query1 ="select id, title from title";
  $title_data1 = custom_query($title_query1);
  
  foreach ($title_data1 as $key => $row){
    $title_id = $row['id'];
    $title = $row['title'];

    
    ?>
    
    <a href="type.php?title_id=<?=$title_id?>"><?=$title?></a><br>
<?php
    } 
  ?>
      
  

  
  
 
        
  
</form>

  

   
</form>


    
  </div>
 
</div>
</div>
<?php
    if(isset($_POST['choose'])){
      $_SESSION['title'] = $_POST['title'];
    }
?>

</body>
</html>
